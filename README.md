## Edgar Gómez AutoDevops Project
This project was made following the 'Devops with Gitlab' course from Platzi.
In this project I used some templates from Gitlab to add the AutoDevops feature to a project.
A Kubernetes Cluster was made with Google Cloud Platform -  Google Kubernetes Engine to access all
the services sent to production.
All necessary configuration needed was configured correctly.
All the AutoDevops pipelines passed succesfully. 

### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
